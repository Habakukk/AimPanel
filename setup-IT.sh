#!/bin/bash

info() {
    echo -e "\e[92m[*] $1\e[39m"
}

both1() {
    info "Updating operating system..."
    apt-get -y upgrade
    info "Adding keys..."
    wget -O - http://repo.aimpanel.pro/deb/repo.gpg.key | apt-key add -
    info "Adding repositories..."
    echo "deb [arch=amd64] http://repo.aimpanel.pro/deb/wheezy wheezy main" > /etc/apt/sources.list.d/aimpanel.list
    info "Updating the list of repositories..."
    apt-get update
    info "Installing the required components..."
    apt-get -y install curl software-properties-common
}

both2() {
    info "Installing the required components..."
    apt-get -y install aimpanel-app oracle-java8-installer --allow-unauthenticated
    info "Execution of external commands..."
    sudo -u aimpanel /usr/local/aimpanel/app/artisan selfheal:check
    sudo -u aimpanel /usr/local/aimpanel/app/artisan admin:create
    service ssh restart
    info "Installation completed!"
    info "Panel address: http://$(curl -s https://api.ipify.org):3131"
    info "Remember to enter your license key"
    info "You can get it for free on the site http://aimpanel.pro/en/"
    info "Command: aimpanel key set <your_key>"
}

jessie() {
    both1
    info "Adding repositories..."
    add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
    info "Updating the list of repositories..."
    apt-get update
    info "Installing the required components..."
    apt-get -y install sudo psmisc gawk procps mawk
    both2
}

xenial() {
    both1
    info "Adding repositories..."
    add-apt-repository ppa:webupd8team/java
    info "Updating the list of repositories..."
    apt-get update
    info "Installing the required components..."
    apt-get -y install sudo psmisc gawk procps mawk libcurl4-openssl-dev
    both2
}

if [ "$(id -u)" != "0" ]
then
    info "The script must be run with root privileges!"
    exit 1
fi

info "Detecting the operating system version..."

apt-get update
apt-get -y install lsb-release

if [ "$(lsb_release -sc)" = "jessie" ]
then
    info "Detected Debian 8 Jessie - OK!"
    jessie
else
    if [ "$(lsb_release -sc)" = "xenial" ]
    then
        info "Detected Ubuntu 16.04 Xenial Xerus - OK!"
        xenial
    else
        info "Your system is not compatible!"
        exit 1
    fi
fi
