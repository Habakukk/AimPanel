#!/bin/bash

info() {
    echo -e "\e[92m[*] $1\e[39m"
}

both1() {
    info "Betriebssystem wird aktualisiert..."
    apt-get -y upgrade
    info "Schlüssel hinzufügen..."
    wget -O - http://repo.aimpanel.pro/deb/repo.gpg.key | apt-key add -
    info "Repositorys hinzufügen..."
    echo "deb [arch=amd64] http://repo.aimpanel.pro/deb/wheezy wheezy main" > /etc/apt/sources.list.d/aimpanel.list
    info "Aktualisieren der Liste der Repositorys..."
    apt-get update
    info "Installieren Sie die erforderlichen Komponenten..."
    apt-get -y install curl software-properties-common
}

both2() {
    info "Installieren Sie die erforderlichen Komponenten..."
    apt-get -y install aimpanel-app oracle-java8-installer --allow-unauthenticated
    info "Ausführung von externen Befehlen..."
    sudo -u aimpanel /usr/local/aimpanel/app/artisan selfheal:check
    sudo -u aimpanel /usr/local/aimpanel/app/artisan admin:create
    service ssh restart
    info "Installation abgeschlossen!"
    info "Panel Adresse: http://$(curl -s https://api.ipify.org):3131"
    info "Vergessen Sie nicht, Ihren Lizenzschlüssel einzugeben"
    info "Sie können es kostenlos auf der Website erhalten http://aimpanel.pro/en/"
    info "Befehl: aimpanel key set <dein_Schlüssel>"
}

jessie() {
    both1
    info "Repositorys hinzufügen..."
    add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
    info "Aktualisieren der Liste der Repositorys..."
    apt-get update
    info "Installieren Sie die erforderlichen Komponenten..."
    apt-get -y install sudo psmisc gawk procps mawk
    both2
}

xenial() {
    both1
    info "Repositorys hinzufügen..."
    add-apt-repository ppa:webupd8team/java
    info "Aktualisieren der Liste der Repositorys..."
    apt-get update
    info "Installieren Sie die erforderlichen Komponenten..."
    apt-get -y install sudo psmisc gawk procps mawk libcurl4-openssl-dev
    both2
}

if [ "$(id -u)" != "0" ]
then
    info "Das Skript muss mit root-Rechten ausgeführt werden!"
    exit 1
fi

info "Ermitteln der Betriebssystemversion..."

apt-get update
apt-get -y install lsb-release

if [ "$(lsb_release -sc)" = "jessie" ]
then
    info "Erkannt Debian 8 Jessie - OK!"
    jessie
else
    if [ "$(lsb_release -sc)" = "xenial" ]
    then
        info "Erkannt Ubuntu 16.04 Xenial Xerus - OK!"
        xenial
    else
        info "Ihr System ist nicht kompatibel!"
        exit 1
    fi
fi
