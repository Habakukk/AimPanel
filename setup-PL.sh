#!/bin/bash

info() {
    echo -e "\e[92m[*] $1\e[39m"
}

both1() {
    info "Aktualizowanie systemu operacyjnego..."
    apt-get -y upgrade
    info "Dodawanie kluczy..."
    wget -O - http://repo.aimpanel.pro/deb/repo.gpg.key | apt-key add -
    info "Dodawanie repozytoriów..."
    echo "deb [arch=amd64] http://repo.aimpanel.pro/deb/wheezy wheezy main" > /etc/apt/sources.list.d/aimpanel.list
    info "Aktualizowanie listy repozytoriów..."
    apt-get update
    info "Instalowanie wymaganych składników..."
    apt-get -y install curl software-properties-common
}

both2() {
    info "Instalowanie wymaganych składników..."
    apt-get -y install aimpanel-app oracle-java8-installer --allow-unauthenticated
    info "Wykonywanie poleceń zewnętrznych..."
    sudo -u aimpanel /usr/local/aimpanel/app/artisan selfheal:check
    sudo -u aimpanel /usr/local/aimpanel/app/artisan admin:create
    service ssh restart
    info "Instalacja zakończona!"
    info "Adres panelu: http://$(curl -s https://api.ipify.org):3131"
    info "Pamiętaj aby wprowadzić swój klucz licencyjny"
    info "Możesz go otrzymać za darmo na stronie http://aimpanel.pro/pl/"
    info "Komenda: aimpanel key set <twój_klucz>"
}

jessie() {
    both1
    info "Dodawanie repozytoriów..."
    add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
    info "Aktualizowanie listy repozytoriów..."
    apt-get update
    info "Instalowanie wymaganych składników..."
    apt-get -y install sudo psmisc gawk procps mawk
    both2
}

xenial() {
    both1
    info "Dodawanie repozytoriów..."
    add-apt-repository ppa:webupd8team/java
    info "Aktualizowanie listy repozytoriów..."
    apt-get update
    info "Instalowanie wymaganych składników..."
    apt-get -y install sudo psmisc gawk procps mawk libcurl4-openssl-dev
    both2
}

if [ "$(id -u)" != "0" ]
then
    info "Skrypt musi zostać uruchomiony z uprawnieniami roota!"
    exit 1
fi

info "Wykrywanie wersji systemu operacyjnego..."

apt-get update
apt-get -y install lsb-release

if [ "$(lsb_release -sc)" = "jessie" ]
then
    info "Wykryto Debian 8 Jessie - OK!"
    jessie
else
    if [ "$(lsb_release -sc)" = "xenial" ]
    then
        info "Wykryto Ubuntu 16.04 Xenial Xerus - OK!"
        xenial
    else
        info "Twój system operacyjny nie jest obsługiwany!"
        exit 1
    fi
fi
