# AimPanel
## YOUR OWN HOSTING PANEL
### Easy and fast way to your private game servers @ VPS or dedicated server

* Site: https://aimpanel.pro/
* Forum: https://forum.lvlup.pro/c/aimpanel
* Topic on Forum: https://forum.lvlup.pro/t/instalacja-aimpanel-na-nowszych-wersjach-systemu-linux/9197

## Install:
``wget https://raw.githubusercontent.com/Habakukk/AimPanel/master/setup-EN.sh --no-check-certificate && chmod +x setup.sh && ./setup.sh``

---
#### Currently available language versions:
* English, 
* Polish, 
* Deutche

#### Language will be added soon:
* French
* Italian
* Russian
* Spanish
* Hebrew
